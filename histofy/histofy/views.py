from django.shortcuts import render
from django.http import HttpResponse

def index(request):
    return render(request, 'landing_page/index.html')

def about_us(request):
    return render(request, 'landing_page/about_us.html')

def contact(request):
    return render(request, 'landing_page/contact.html')
