# Histofy
## _Landing Page_

Landing Page de pré-registro em python utilizando o framework django.

## Como configurar e iniciar o host em sua máquina

- Clone o repositório
- Tenha o Python 3.9 em sua máquina
- Baixe os requisitos no arquivo requirements.txt utilizando o comando: _pip install -r requirements.txt_ ou _pip3 install -r requirements.txt_
- No diretório do repositório clonado execute o comando: _python manage.py runserver_ ou _python3 manage.py runserver_

